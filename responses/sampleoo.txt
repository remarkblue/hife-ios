{
    device =     {
        description = "";
        "first_activity" = "2016-03-20T04:40:27.000Z";
        id = 3;
        "last_activity" = "2016-03-20T05:06:18.000Z";
        "member_id" = "<null>";
        metadata = "";
        name = "iPhone (2)";
        "status_id" = 14;
        "system_name" = "iPhone OS";
        uuid = "79DE4EA6-65E1-475E-A5F8-DB6303F257B1";
        version = "9.2.1";
    };
    meeting =     {
        description = "<null>";
        "first_activity" = "2016-03-20T04:40:27.000Z";
        id = 7;
        "last_activity" = "2016-03-20T05:06:18.000Z";
        latitude = "43.6123491254152";
        longitude = "-116.169077912774";
        "meeting_type_id" = "<null>";
        metadata = "<null>";
        name = "Boise Mar 19, 2016";
        ranking = "<null>";
        size = 22;
        "spot_id" = "<null>";
        "status_id" = 14;
    };
    "moment_metric" =     {
        agree = 4;
        cool = 2;
        disagree = 4;
        what = 2;
    };
    neighbors =     (
    );
    spot =     {
        "device_id" = 3;
        "first_activity" = "2016-03-20T05:06:18.000Z";
        id = 87;
        "last_activity" = "2016-03-20T05:06:18.000Z";
        latitude = "43.6123971781168";
        longitude = "-116.169057497863";
        "meeting_id" = 7;
        metadata = "<null>";
        name = "<null>";
        ranking = "<null>";
        "spot_type_id" = 0;
        "status_id" = 1;
        url = "<null>";
    };
    "spot_type" = in;
}
