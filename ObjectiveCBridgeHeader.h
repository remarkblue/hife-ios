//
//  ObjectiveCBridgeHeader.h
//  hife
//
//  Created by MAURICIO MARTINEZ INGUANZO on 2/20/16.
//  Copyright © 2016 inguansoft. All rights reserved.
//

#ifndef ObjectiveCBridgeHeader_h
#define ObjectiveCBridgeHeader_h

#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>
#import <AWSCognito/AWSCognito.h>

#endif /* ObjectiveCBridgeHeader_h */
