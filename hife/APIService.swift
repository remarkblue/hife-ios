import MapKit
import Alamofire

class APIService {
    
    private init() {
    }
    
    static let Instance = APIService()
    
    private func _postSpot(spotTypeId:Int,
                           completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void){
        let location:CLLocation = LocationService.Instance.current
        let geol = LocationService.Instance.geoLocation
        if(geol != nil) {
            
            var params = [
                "device":[
                    "uuid": SettingsService.Instance.deviceUUID!,
                    "name": SettingsService.Instance.deviceName!,
                    "system_name": SettingsService.Instance.deviceSystemName!,
                    "version": SettingsService.Instance.deviceSystemVersion!,
                    "time": "\(Int(NSDate().timeIntervalSince1970))"
                ],
                "spot":[
                    "city":     "\(geol["city"]!)",
                    "state":    "\(geol["state"]!)",
                    "country":  "\(geol["country"]!)",
                    "latitude": "\(location.coordinate.latitude)" ,
                    "longitude": "\(location.coordinate.longitude)",
                    "spot_type_id": "\(spotTypeId)",
                    "timestamp": "\(Int(NSDate().timeIntervalSince1970))"
                ]]
            
            if MeetingService.Instance.current != nil {
                params["meeting"]  = [
                    "guid": MeetingService.Instance.current.guid
                ]
            }
            
            Alamofire.request(.POST, InternetService.Instance.endPointURL("spot"),
                parameters: params)
                .responseJSON { response in
                    if let JSON = response.result.value {
                        completionHandler(JSON as? NSDictionary, nil, nil)
                        //self.current.update(JSON as! NSDictionary)
                    }
            }
        } else {
            print("work offline coming up soon...");
        }
    }
    
    func getDeviceMeetings(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        if SettingsService.Instance.deviceUUID != nil {
        Alamofire.request(.GET, InternetService.Instance.endPointURL("device/\(SettingsService.Instance.deviceUUID!)/meetings"),
            parameters: [
                "device":[
                    "uuid": SettingsService.Instance.deviceUUID!,
                    "name": SettingsService.Instance.deviceName!,
                    "system_name": SettingsService.Instance.deviceSystemName!,
                    "version": SettingsService.Instance.deviceSystemVersion!,
                    "time": "\(Int(NSDate().timeIntervalSince1970))"
                ]]
            )
            .responseJSON { response in
                if let JSON = response.result.value {
                    completionHandler(JSON as? NSDictionary, nil, nil)
                    //self.current.update(JSON as! NSDictionary)
                }
            }
        }
    }
    
    func getDiscoverMeetings(query:String, completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void){
        if SettingsService.Instance.deviceUUID != nil {
            Alamofire.request(.GET, InternetService.Instance.endPointURL("search"),
                parameters: [
                    "q": query]
                )
                .responseJSON { response in
                    if let JSON = response.result.value {
                        completionHandler(JSON as? NSDictionary, nil, nil)
                        //self.current.update(JSON as! NSDictionary)
                    }
            }
        }
    }
    
    func getJoinableMeetings(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        let location:CLLocation = LocationService.Instance.current
        let geol = LocationService.Instance.geoLocation
        Alamofire.request(.GET, InternetService.Instance.endPointURL("joinable-meetings"),
            parameters: [
                "device":[
                    "uuid": SettingsService.Instance.deviceUUID!,
                    "name": SettingsService.Instance.deviceName!,
                    "system_name": SettingsService.Instance.deviceSystemName!,
                    "version": SettingsService.Instance.deviceSystemVersion!,
                    "time": "\(Int(NSDate().timeIntervalSince1970))"
                ],
                "spot":[
                    "city":     "\(geol["city"]!)",
                    "state":    "\(geol["state"]!)",
                    "country":  "\(geol["country"]!)",
                    "latitude": "\(location.coordinate.latitude)" ,
                    "longitude": "\(location.coordinate.longitude)",
                    "timestamp": "\(Int(NSDate().timeIntervalSince1970))"
                ]]
            )
            .responseJSON { response in
                if let JSON = response.result.value {
                    completionHandler(JSON as? NSDictionary, nil, nil)
                    //self.current.update(JSON as! NSDictionary)
                }
        }
    }
    
    func bindDevice(token:String, completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void){
            Alamofire.request(.POST, InternetService.Instance.endPointURL("bind-device/\(token )"),
                parameters: [
                    "device":[
                        "uuid": SettingsService.Instance.deviceUUID!,
                        "name": SettingsService.Instance.deviceName!,
                        "system_name": SettingsService.Instance.deviceSystemName!,
                        "version": SettingsService.Instance.deviceSystemVersion!,
                        "time": "\(Int(NSDate().timeIntervalSince1970))"
                    ]
                ])
                .responseJSON { response in
                    if let JSON = response.result.value {
                        completionHandler(JSON as? NSDictionary, nil, nil)
                        //self.current.update(JSON as! NSDictionary)
                    }
            }
    }
    
    func joinSpot(meetingGuid:String, completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void){
        let location:CLLocation = LocationService.Instance.current
        let geol = LocationService.Instance.geoLocation
        if(geol != nil) {
            Alamofire.request(.POST, InternetService.Instance.endPointURL("join-spot"),
                parameters: [
                    "meeting":[
                        "guid": meetingGuid
                    ],
                    "device":[
                        "uuid": SettingsService.Instance.deviceUUID!,
                        "name": SettingsService.Instance.deviceName!,
                        "system_name": SettingsService.Instance.deviceSystemName!,
                        "version": SettingsService.Instance.deviceSystemVersion!,
                        "time": "\(Int(NSDate().timeIntervalSince1970))"
                    ],
                    "spot":[
                        "city":     "\(geol["city"]!)",
                        "state":    "\(geol["state"]!)",
                        "country":  "\(geol["country"]!)",
                        "latitude": "\(location.coordinate.latitude)" ,
                        "longitude": "\(location.coordinate.longitude)",
                        "spot_type_id": "0",
                        "timestamp": "\(Int(NSDate().timeIntervalSince1970))"
                    ]])
                .responseJSON { response in
                    if let JSON = response.result.value {
                        completionHandler(JSON as? NSDictionary, nil, nil)
                        //self.current.update(JSON as! NSDictionary)
                    }
            }
        } else {
            print("work offline coming up soon...");
        }
    }
    
    
    func newMeetingSpot(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        let location:CLLocation = LocationService.Instance.current
        let geol = LocationService.Instance.geoLocation
        if(geol != nil) {
            Alamofire.request(.POST, InternetService.Instance.endPointURL("create-new-meeting-spot"),
                parameters: [
                    "device":[
                        "uuid": SettingsService.Instance.deviceUUID!,
                        "name": SettingsService.Instance.deviceName!,
                        "system_name": SettingsService.Instance.deviceSystemName!,
                        "version": SettingsService.Instance.deviceSystemVersion!,
                        "time": "\(Int(NSDate().timeIntervalSince1970))"
                    ],
                    "spot":[
                        "city":     "\(geol["city"]!)",
                        "state":    "\(geol["state"]!)",
                        "country":  "\(geol["country"]!)",
                        "latitude": "\(location.coordinate.latitude)" ,
                        "longitude": "\(location.coordinate.longitude)",
                        "spot_type_id": "0",
                        "timestamp": "\(Int(NSDate().timeIntervalSince1970))"
                    ]])
                .responseJSON { response in
                    if let JSON = response.result.value {
                        completionHandler(JSON as? NSDictionary, nil, nil)
                        //self.current.update(JSON as! NSDictionary)
                    }
            }
        } else {
            print("work offline coming up soon...");
        }
    }
    
    
    func inSpot(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        _postSpot(0, completionHandler: completionHandler)
    }
    
    func coolSpot(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        _postSpot(5, completionHandler: completionHandler)
    }
    
    func whatSpot(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        _postSpot(2, completionHandler: completionHandler)
    }
    
    func agreeSpot(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        _postSpot(4, completionHandler: completionHandler)
    }
    
    func disagreeSpot(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        _postSpot(3, completionHandler: completionHandler)
    }
    
    func captureAudioSpot(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        _postSpot(20, completionHandler: completionHandler)
    }
    
    func captureVideoSpot(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        _postSpot(21, completionHandler: completionHandler)
    }
    
    func capturePhotoSpot(completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        _postSpot(22, completionHandler: completionHandler)
    }
    
    func updateSpotLastActivity(spotId:String, completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void){
        let location:CLLocation = LocationService.Instance.current
        InternetService.Instance.put([
            "spot":[
                "guid": "\(spotId)",
                "latitude": "\(location.coordinate.latitude)" ,
                "longitude": "\(location.coordinate.longitude)",
                "timestamp": "\(Int(NSDate().timeIntervalSince1970))"
            ]], endPoint: "spot", completionHandler: completionHandler)
    }
    
    func deleteSpot(spotId:String, completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void){
        InternetService.Instance.delete([
            "spot":[
                "guid": "\(spotId)"
            ]], endPoint: "spot/\(spotId)", completionHandler: completionHandler)
    }
    
    func mediaSpotUploaded(key:String, completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        InternetService.Instance.post([ "uploaded":[ "key": key ] ],
                                      endPoint: "spot-uploaded", completionHandler: completionHandler)
    }
}