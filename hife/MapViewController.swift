import UIKit
import MapKit

class MapViewController: UIViewController, LocationServiceDelegate {
    
    @IBOutlet var mapView: MKMapView!
    var annotations: [MKPointAnnotation]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if LocationService.Instance.state > 0 {
            locationServiceReady()
        } else {
            LocationService.Instance.delegate = self
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func locationServiceReady(){
        let location = LocationService.Instance.current
        
        annotations = [MKPointAnnotation]()
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location!.coordinate
        annotations.insert(annotation, atIndex: annotations.count)
        
        let region = MKCoordinateRegionMakeWithDistance(annotation.coordinate,
            1200, 1200)
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func outMap() {
        performSegueWithIdentifier("outMap", sender: nil)
    }
    
}

