import Alamofire

class MeetingService {
    
    var current:Meeting!
    
    private init() {
    }
    
    static let Instance = MeetingService()
    
    func hook(meetingData:NSDictionary) {
        current = Meeting(meetingData: meetingData)
    }
    
    func refreshMeeting(){
        if (self.current != nil) {
            Alamofire.request(.GET, InternetService.Instance.endPointURL("meeting/\(self.current.guid)"),
                parameters: [
                    "time": "\(Int(NSDate().timeIntervalSince1970))"
                ])
                .responseJSON { response in
                    if let JSON = response.result.value {
                        if self.current != nil {
                            self.current.update(JSON as! NSDictionary)
                        }
                    }
            }
        }
    }
    
    func closeCurrentMeeting() {
        if (self.current != nil) {
            Alamofire.request(.PUT, InternetService.Instance.endPointURL("meeting/\(self.current.guid)"),
                parameters: [
                    //TODO: change the 5 for a nice constant
                    "status_id": 5
                ])
                .responseJSON { response in
                    self.current = nil
            }
        }
    }
    
    func updateMeetingName(meetingName:String) {
        if (self.current != nil) {
            Alamofire.request(.PUT, InternetService.Instance.endPointURL("meeting/\(self.current.guid)"),
                parameters: [
                    "name": meetingName
                ])
                .responseJSON { response in
                    if let JSON = response.result.value {
                        self.current.update(JSON as! NSDictionary)
                    }
            }
        }
    }
    
}