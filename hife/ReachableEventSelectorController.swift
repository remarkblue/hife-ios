import UIKit

class ReachableEventSelectorController: UITableViewController {
    
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    var activityItem = UIBarButtonItem()
    
    private var _meetings:[Meeting]!
    
    private var _query:String!
    
    private func _submitSearch() {
        APIService.Instance.getDiscoverMeetings(_query, completionHandler: {data, response, error -> Void in
            self._meetings = []
            if(data!["object"] != nil && data!["object"]?.count > 0) {
                let meetings = data!["object"] as! [Dictionary<String, AnyObject>]
                for item in meetings {
                    let meeting = Meeting(meetingData: item)
                    self._meetings.append(meeting)
                }
            }
            self.tableView.reloadData()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
        activityIndicator.hidden = false
        self.activityItem = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.rightBarButtonItem = activityItem
        self.navigationController!.navigationBar.hidden = false // for navigation bar hide
        self.navigationItem.hidesBackButton = MeetingService.Instance.current == nil
        
        _meetings = []
        
        _query = ""
        _submitSearch()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _meetings.count + 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        switch indexPath.row {
        case 0:
            if self._query == "" {
                cell.textLabel?.text = "Search"
            } else {
                cell.textLabel?.text = "Search (\(self._query))"
            }
        default:
            cell.textLabel?.text = "\(_meetings[indexPath.row-1].neighbors.count) / \(_meetings[indexPath.row-1].name)"
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            let alert = UIAlertController(title: "Search", message: "Find the meeting you need to impact", preferredStyle: .Alert)
            
            alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
                textField.text = ""
            })
            
            alert.addAction(UIAlertAction(title: "Search", style: .Default, handler: { (action) -> Void in
                let textField = alert.textFields![0] as UITextField
                self._query = textField.text!
                self._submitSearch()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel) { (_) in })
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
            performSegueWithIdentifier("playEvent", sender: nil)
        }
    }
    
}
