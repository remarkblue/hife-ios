import UIKit

class JoinedEventSelectorController: UITableViewController {
    
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    var activityItem = UIBarButtonItem()
    
    private var _meetings:[Meeting]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
        activityIndicator.hidden = false
        self.activityItem = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.rightBarButtonItem = activityItem
        self.navigationController!.navigationBar.hidden = false // for navigation bar hide
        self.navigationItem.hidesBackButton = MeetingService.Instance.current == nil
        
        _meetings = []
        
        APIService.Instance.getDeviceMeetings({data, response, error -> Void in
            self._meetings = []
            if(data!["object"] != nil && data!["object"]?.count > 0) {
                let meetings = data!["object"] as! [Dictionary<String, AnyObject>]
                for item in meetings {
                    let meeting = Meeting(meetingData: item)
                    self._meetings.append(meeting)
                }
            }
            self.tableView.reloadData()
        })
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _meetings.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "\(_meetings[indexPath.row].neighbors.count) / \(_meetings[indexPath.row].name)"
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("playEvent", sender: nil)
    }
    
}
