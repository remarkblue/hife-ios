import Foundation

class Meeting {
    
    var id:Int
    var guid:String
    
    var description:String
    var first_activity:NSDate!
    var last_activity:NSDate!
    var latitude:CGFloat!
    var longitude:CGFloat!
    var meeting_type_id: Int!
    var metadata:String!
    var name:String!
    var status_id:Int!
    
    var neighbors:[Device]
    
    var whatMomentMetric:Int
    var coolMomentMetric:Int
    var agreeMomentMetric:Int
    var disagreeMomentMetric:Int
    
    init(meetingData:NSDictionary){
        //        let dateFormatter : NSDateFormatter = NSDateFormatter()
        //        print(meetingData)
        if(meetingData["meeting"] != nil && meetingData["meeting"]!["guid"] != nil) {
            id = meetingData["meeting"]!["id"] as! Int
            guid = meetingData["meeting"]!["guid"] as! String
            if let desc:String = meetingData["meeting"]!["description"] as? String {
                description = desc
            } else {
                description = "Description not specified yet"
            }
            
            name = meetingData["meeting"]!["name"] as! String
            status_id = meetingData["meeting"]!["status_id"] as! Int
            
            //        first_activity = dateFormatter.dateFromString(
            //            meetingData["meeting"]!["first_activity"] as! String)!
            //
            //        last_activity = dateFormatter.dateFromString(
            //            meetingData["meeting"]!["last_activity"] as! String)!
            //
            //        latitude = meetingData["meeting"]!["latitude"] as! CGFloat
            //        longitude = meetingData["meeting"]!["longitude"] as! CGFloat
            //        meeting_type_id = meetingData["meeting"]!["meeting_type_id"] as!  Int
            //        metadata = meetingData["meeting"]!["metadata"] as! String
            //        name = meetingData["meeting"]!["name"] as! String
            
            
            neighbors = []
            if meetingData["devices"] != nil {
                let theseNeighbors = meetingData["devices"]! as! NSArray
                for thisNeighbor in theseNeighbors {
                    neighbors.append(Device(deviceData: thisNeighbor as! NSDictionary));
                }
            }
            
            whatMomentMetric = meetingData["moment_metric"]!["what"] as! Int
            coolMomentMetric = meetingData["moment_metric"]!["cool"] as! Int
            agreeMomentMetric = meetingData["moment_metric"]!["agree"] as! Int
            disagreeMomentMetric = meetingData["moment_metric"]!["disagree"] as! Int
        } else {
            neighbors = []
            description = ""
            id=0
            guid = ""
            whatMomentMetric = 0
            coolMomentMetric = 0
            agreeMomentMetric = 0
            disagreeMomentMetric = 0
        }
    }
    
    func update(meetingData:NSDictionary){
        name = meetingData["meeting"]!["name"] as! String
        
        status_id = meetingData["meeting"]!["status_id"] as! Int
        
        whatMomentMetric = meetingData["moment_metric"]!["what"] as! Int
        coolMomentMetric = meetingData["moment_metric"]!["cool"] as! Int
        agreeMomentMetric = meetingData["moment_metric"]!["agree"] as! Int
        disagreeMomentMetric = meetingData["moment_metric"]!["disagree"] as! Int
    }
    
    
}