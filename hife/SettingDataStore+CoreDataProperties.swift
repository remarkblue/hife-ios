//
//  SettingDataStore+CoreDataProperties.swift
//  hife
//
//  Created by MAURICIO MARTINEZ INGUANZO on 4/10/16.
//  Copyright © 2016 inguansoft. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SettingDataStore {

    @NSManaged var audio_time_out: NSNumber?
    @NSManaged var upload_only_wifi: NSNumber?
    @NSManaged var device_uuid: String?

}
