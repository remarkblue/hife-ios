import AVFoundation

class AudioService :  NSObject, AVAudioPlayerDelegate,
AVAudioRecorderDelegate{
    
    private var audioRecorder: AVAudioRecorder!
    
    private var activeSpotId:String!
    
    private var _cancelState:Bool = false
    
    var delegate:AudioServiceDelegate!
    
    private override init() {
        
    }
    
    static let Instance = AudioService()
    
    
    func recording() -> Bool {
        if(audioRecorder == nil) {
            return false
        } else {
            return (audioRecorder?.recording)!
        }
    }
    
    func disregard() {
        if(audioRecorder != nil && audioRecorder?.recording == true) {
            _cancelState = true
            audioRecorder?.stop()
            self.delegate?.recordingStopped()
        } else {
            print("Nothing to cancel")
        }
    }
    
    func stop() {
        if audioRecorder?.recording == true {
            audioRecorder?.stop()
            
            let audioSession = AVAudioSession.sharedInstance()
            do {
                try audioSession.setActive(false)
            } catch {
                print("AudioSession Inactive Failed !!!!")
            }
        } else {
            print("Unexpected stop when recorder was not running!")
        }
    }
    
    func startAudioCapture() {
        if ((audioRecorder?.recording) == true) {
            print("unexpected start recording on session running at the service");
        } else {
            
            APIService.Instance.captureAudioSpot({data, response, error -> Void in
                if(data != nil && data!["spot"] != nil){
                    MeetingService.Instance.hook(data!)
                    
                    self.activeSpotId = String(data!["spot"]!["guid"])
                    let recordingName = self.activeSpotId + ".wav"
                    let filename:String = HelperService.Instance.getDocumentsDirectoryPath(recordingName)
                    let filePath = NSURL(fileURLWithPath: filename)
                    
                    //Create a session
                    let session=AVAudioSession.sharedInstance()
                    
                    //TODO: check if we can record already encoded to avoid transcoding AWS service
                    let recordSettings:[String:AnyObject] = [
                        AVEncoderAudioQualityKey: AVAudioQuality.Min.rawValue,
                        AVEncoderBitRateKey: 16,
                        AVNumberOfChannelsKey: 1,
                        AVSampleRateKey: 16000.0]
                    do {
                        try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
                        //Create a new audio recorder
                        try self.audioRecorder = AVAudioRecorder(URL: filePath,
                            settings: recordSettings)
                        self.audioRecorder.delegate = self
                        self.audioRecorder.meteringEnabled=true
                        self.audioRecorder.prepareToRecord()
                        self.audioRecorder.record()
                        self.delegate?.recordingStarted()
                    } catch {
                        print("AudioSession Failed !!!!")
                        self.delegate?.recordingFailToStart()
                    }
                    
                } else {
                    self.delegate?.recordingFailToStart()
                }
            })
        }
    }
    
    
    //For Play
    //    audioPlayer = AVAudioPlayer(contentsOfURL: audioRecorder?.url,
    //    error: &error)
    //
    //    audioPlayer?.delegate = self
    //
    //    if let err = error {
    //        println("audioPlayer error: \(err.localizedDescription)")
    //    } else {
    //    audioPlayer?.play()
    //audioPlayer?.stop()
    //    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        
    }
    
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: NSError?) {
        print("Audio Play Decode Error")
    }
    
    
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder, successfully flag: Bool) {
        //print("calledBack Audio completed!")
        if(_cancelState == true) {
                print("cancelState!")
            APIService.Instance.deleteSpot(self.activeSpotId, completionHandler: {data, response, error -> Void in
            })
            let fileManager = NSFileManager.defaultManager()
            do {
                try fileManager.removeItemAtPath(recorder.url.path!)
            }
            catch let error as NSError {
                print("Ooops! Something went wrong: \(error)")
            }
        } else {
            if(flag) {
                S3Service.Instance.upload(
                    recorder.url.path!, suffix: recorder.url.lastPathComponent!)
                print("Upload requested!")
                APIService.Instance.updateSpotLastActivity(self.activeSpotId, completionHandler: {data, response, error -> Void in
                    print("TimeStamp on spot updated!")
                })
                self.delegate?.recordingStopped()
                self.activeSpotId = nil
            } else {
                print("recording not successful")
            }
        }
        _cancelState = false
    }
    
    func audioRecorderEncodeErrorDidOccur(recorder: AVAudioRecorder, error: NSError?) {
        print("Audio Record Encode Error")
    }
    
}
