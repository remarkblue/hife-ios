import Foundation

class Device {
    
    var id:Int
    var name:String!
    var system_name:String!
    var description:String!
    var first_activity:NSDate!
    var last_activity:NSDate!
    var status_id:Int
    var uuid:String
    var version:String
    
    
    init(deviceData:NSDictionary){
//        let dateFormatter : NSDateFormatter = NSDateFormatter()
        
        id = deviceData["id"] as! Int
        name = deviceData["name"] as! String
        system_name = deviceData["system_name"] as! String
        description = deviceData["description"] as! String

//        first_activity = dateFormatter.dateFromString(
//            deviceData["first_activity"] as! String)!
//        last_activity = dateFormatter.dateFromString(
//            deviceData["last_activity"] as! String)!
        
        status_id = deviceData["status_id"] as! Int
        uuid = deviceData["uuid"] as! String
        version = deviceData["version"] as! String
    }
}