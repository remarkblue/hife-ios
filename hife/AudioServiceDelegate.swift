protocol AudioServiceDelegate {
    func recordingStarted()
    func recordingStopped()
    func recordingFailToStart()
}