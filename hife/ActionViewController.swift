import UIKit
import MobileCoreServices
import AVFoundation

class ActionViewController:
    UIViewController,
    UINavigationControllerDelegate,
    UIImagePickerControllerDelegate,
    AudioServiceDelegate,
    S3ServiceDelegate
{
    @IBOutlet var noInternet: UIImageView!
    
    @IBOutlet var uploadLabel: UILabel!
    @IBOutlet var uploadActivity: UIActivityIndicatorView!
    
    @IBOutlet var statusButton: UIButton!
    @IBOutlet var gearButton: UIButton!
    
    @IBOutlet var coolButton: UIButton!
    @IBOutlet var audioButton: UIButton!
    @IBOutlet var photoButton: UIButton!
    
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var audioRunningButton: UIButton!
    @IBOutlet var activeRecLabel: UILabel!
    
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var progressBlock: AWSS3TransferUtilityUploadProgressBlock?
    
    private let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC))),
    dispatchFilterTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC))),
    coolPressedImage:UIImage! = UIImage(named: "hife_100"),
    coolImage:UIImage! = UIImage(named: "hife_70"),
    
    gearImage:UIImage! = UIImage(named: "gear_32"),
    stopImage:UIImage! = UIImage(named: "stop")
    
    var filePath:NSURL!
    var counter:Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if LocationService.Instance.state == 0 {
            uploadActivity.hidden = true
            uploadLabel.hidden = true
        }
        
        
        //        self.progressBlock = {(task, progress) in
        //            dispatch_async(dispatch_get_main_queue(), {
        //                self.progressView.progress = Float(progress.fractionCompleted)
        //                self.statusLabel.text = "Uploading..."
        //            })
        //        }
        
        self.completionHandler = { (task, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                if (error != nil){
                    print("Failed with error")
                    print("Error: \(error!)")
                }
                    //                else
                    //                    if(self.progressView.progress != 1.0) {
                    //                        self.statusLabel.text = "Failed"
                    //                        NSLog("Error: Failed - Likely due to invalid region / filename")
                    //                    }
                else{
                    print("Success!")
                }
            })
        }
        
        
        
        
        
        AudioService.Instance.delegate = self
        S3Service.Instance.delegate = self
    }
    
    @IBAction func coolSpot(){
        dispatch_after(dispatchFilterTime, dispatch_get_main_queue(), {
            self.coolButton.setImage(self.coolPressedImage, forState: UIControlState.Normal)
            dispatch_after(self.dispatchTime, dispatch_get_main_queue(), {
                self.coolButton.setImage(self.coolImage, forState: UIControlState.Normal)
            })
        })
        APIService.Instance.coolSpot({data, response, error -> Void in
            MeetingService.Instance.hook(data!)
        })
    }
    
    @IBAction func startAudioCapture() {
        if(AudioService.Instance.recording()) {
            print("Unexpected start when already running recording session");
        } else {
            _setUIToAudioRecording()
            AudioService.Instance.startAudioCapture();
            
        }
    }
    
    @IBAction func completeAudioCapture() {
        if(AudioService.Instance.recording()) {
            
            
            let alert = UIAlertController(title: "Confirm stop recording", message: "To avoid accidental taps on the screen we ask you to confirm recording stop.  Your recording will be saved.", preferredStyle: .Alert)
            
            alert.addAction(UIAlertAction(title: "Stop and Save", style: .Default, handler: { (action) -> Void in
                AudioService.Instance.stop()
                self._backToNoRecording()
            }))
            alert.addAction(UIAlertAction(title: "Close", style: .Cancel) { (_) in })
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
            print("Unexpected stop when no running recording session");
            _backToNoRecording()
        }
        
    }
    
    @IBAction func settingsMenu(){
        var alert:UIAlertController
        
        if MeetingService.Instance.current == nil {
            alert = UIAlertController(title: "Offline Session", message: "You are not connected to any meeting for missing internet connection.  Your data will need to be migrated manually.",
                                      preferredStyle: UIAlertControllerStyle.ActionSheet)
        } else {
            alert = UIAlertController(title: nil, message: nil,
                                      preferredStyle: UIAlertControllerStyle.ActionSheet)
        }
        
        
        alert.popoverPresentationController?.sourceView = gearButton
        alert.popoverPresentationController?.sourceRect = gearButton.bounds
        // this is the center of the screen currently but it can be any point in the view
        
        
        alert.addAction(UIAlertAction(title: "Choose another event", style: .Default)
        { (_) in self.anotherEvent() })
        
        if MeetingService.Instance.current != nil {
            
            alert.addAction(UIAlertAction(title: "Update Event Name", style: .Default)
            { (_) in self.updateMeetingName() })
            
            
            //TODO: only adins should be able to do this
            alert.addAction(UIAlertAction(title: "Close Current Event", style: .Default)
            { (_) in self.closeMeeting() })
            
            alert.addAction(UIAlertAction(title: "Web", style: .Default)
            { (_) in self.goWebMeeting() })
            
            alert.addAction(UIAlertAction(title: "Map", style: .Default)
            { (_) in self.goMap() })
        }
        
        if(SettingsService.Instance.getWifiSetting()) {
            alert.addAction(UIAlertAction(title: "Set Upload anyway possible", style: .Default)
            { (_) in self.uploadAny() })
        } else {
            alert.addAction(UIAlertAction(title: "Set Upload Only WiFi", style: .Default)
            { (_) in self.onlyWifiUpload() })
        }
        
        alert.addAction(UIAlertAction(
            title: "Rec. Background \(Int(SettingsService.Instance.getAudioTimeoutSetting()))min", style: .Default)
        { (_) in self.changeAudioTimeout() })
        
        alert.addAction(UIAlertAction(title: "Hife & Your data", style: .Default)
        { (_) in self.hifeData() })
        
        alert.addAction(UIAlertAction(title: "Close", style: .Cancel) { (_) in })
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func changeAudioTimeout(){
        let alert = UIAlertController(title: "Background Timeout", message: "Set the desired minutes timeout you want hife to stop recording when running in background", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.text = "\(Int(SettingsService.Instance.getAudioTimeoutSetting()))"
        })
        
        alert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            SettingsService.Instance.setAudioTimeoutSetting(Int(textField.text!)! as NSNumber)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel) { (_) in })
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func confirmAudioCancel() {
        let alert = UIAlertController(title: "Recording will be lost", message: "Are you sure you want to disregard the current audio recording?  Your recording will be lost.", preferredStyle: .Alert)
        
        alert.addAction(UIAlertAction(title: "Cancel Recording", style: .Default, handler: { (action) -> Void in
            self._backToNoRecording()
            AudioService.Instance.disregard()
        }))
        alert.addAction(UIAlertAction(title: "Close", style: .Cancel) { (_) in })
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func anotherEvent(){
        performSegueWithIdentifier("goChooseEvent", sender: nil)
    }
    
    func onlyWifiUpload(){
        SettingsService.Instance.setWifiSetting(true)
    }
    
    func uploadAny(){
        SettingsService.Instance.setWifiSetting(false)
    }
    
    func hifeData(){
        performSegueWithIdentifier("goData", sender: nil)
    }
    
    @IBAction func updateMeetingNameAction() {
        updateMeetingName()
    }
    
    func updateMeetingName() {
        if MeetingService.Instance.current != nil && InternetService.Instance.isHot() {
            let alert = UIAlertController(title: "Update Event",
                                          message: "Customize the event name, It will be immediatelly visible to all hifers at your event!", preferredStyle: .Alert)
            
            alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
                textField.text = MeetingService.Instance.current.name
            })
            
            alert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action) -> Void in
                let textField = alert.textFields![0] as UITextField
                MeetingService.Instance.updateMeetingName(textField.text!)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel) { (_) in })
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func closeMeeting(){
        if AudioService.Instance.recording() {
            let alert = UIAlertController(title: "Hife is recording", message: "Please, stop recording before closing this event", preferredStyle: .Alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .Cancel) { (_) in })
            
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Closing Event Activity", message: "Are you sure you want to close the event, no more activities will be allowed from present attendes", preferredStyle: .Alert)
            
            alert.addAction(UIAlertAction(title: "Close Event", style: .Default, handler: { (action) -> Void in
                MeetingService.Instance.closeCurrentMeeting()
                self.anotherEvent()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel) { (_) in })
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func goWebMeeting() {
        if let url = NSURL(string: "https://hife.io/meeting/\(MeetingService.Instance.current.guid)") {
            UIApplication.sharedApplication().openURL(url)
        } else {
            let _ = UIAlertController(title: "Ops, pls try again", message: "Hife is creating the meeting, please try again.",
                                      preferredStyle: UIAlertControllerStyle.ActionSheet)
        }
    }
    
    func goMap() {
        performSegueWithIdentifier("goMap", sender: nil)
    }
    
    private var timer:NSTimer!
    
    func refreshMeeting(){
        MeetingService.Instance.refreshMeeting()
        
        if MeetingService.Instance.current != nil && MeetingService.Instance.current.status_id! == 5 {
            if AudioService.Instance.recording() {
                AudioService.Instance.stop()
                self._backToNoRecording()
            }
            MeetingService.Instance.current = nil
            performSegueWithIdentifier("goChooseEvent", sender: nil)
        }
        
        _repaintActivityCircles()
        if InternetService.Instance.isHot() {
            if coolButton.hidden == true {
                _enableUI()
            }
        } else {
            _disableUI()
        }
        
        if counter > 6 {
            InternetService.Instance.updateNetworkStatus()
            S3Service.Instance.retryPendingUploads()
            counter = 0
        }
        counter = counter + 1
    }
    
    private func _paintCircleOn(areaView: UIButton, spotType:String){
        var value:Int = 0
        if(MeetingService.Instance.current != nil) {
            switch(spotType) {
            case "cool":
                value = MeetingService.Instance.current.coolMomentMetric
            default:
                value = 0
            }
        }
        
        if areaView.layer.sublayers?.count > 1 {
            areaView.layer.sublayers?.removeLast()
        }
        if (value > 0) {
            let width:CGFloat = areaView.layer.frame.size.width,
            height:CGFloat = areaView.layer.frame.size.height,
            positionX:CGFloat = width/2 - 20,
            positionY:CGFloat = height/2 + 35
            
            let textLayer = CATextLayer()
            
            textLayer.frame = CGRect(x: positionX, y: positionY,
                                     width: 100, height: 20)
            textLayer.string = "\(value)"
            textLayer.font = CTFontCreateWithName(
                kCMTextMarkupGenericFontName_Casual,
                16, nil)
            textLayer.fontSize = 14
            
            areaView.layer.addSublayer(textLayer)
        }
    }
    
    private func _repaintActivityCircles(){
        if(MeetingService.Instance.current != nil) {
            statusButton.setTitle(MeetingService.Instance.current.name, forState: .Normal)
            //            _paintCircleOn(agreeButton, spotType: "agree")
            //            _paintCircleOn(disagreeButton, spotType: "disagree")
            //            _paintCircleOn(whatButton, spotType: "what")
            _paintCircleOn(coolButton, spotType: "cool")
        } else {
            statusButton.setTitle("Connecting...", forState: .Normal)
        }
    }
    
    private func _disableUI(){
        coolButton.hidden = true
        audioButton.enabled = false
        photoButton.enabled = false
        noInternet.hidden = false
        statusButton.setTitle("offline", forState: .Normal)
    }
    
    private func _enableUI(){
        LocationService.Instance.postMeetingEntrance()
        coolButton.hidden = false
        audioButton.enabled = true
        photoButton.enabled = true
        noInternet.hidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController!.navigationBar.hidden = true // for navigation bar hide
        
        if AudioService.Instance.recording() {
            _setUIToAudioRecording()
        } else{
            _backToNoRecording()
        }
        
        currentUploadsChanged()
        
        _repaintActivityCircles()
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: "orientationChanged",
                                                         name: UIDeviceOrientationDidChangeNotification, object: nil)
        InternetService.Instance.updateNetworkStatus()
        
        if InternetService.Instance.isHot() {
            if coolButton.hidden == true {
                _enableUI()
            }
        } else {
            _disableUI()
        }
        
        if timer != nil {
            timer.invalidate()
        }
        timer = NSTimer.scheduledTimerWithTimeInterval(3.0,
                                                       target: self,
                                                       selector: "refreshMeeting",
                                                       userInfo: nil, repeats: true)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        if timer != nil {
            timer.invalidate()
        }
    }
    
    func orientationChanged() {
        _repaintActivityCircles()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //----------------------------------------- Photo
    @IBAction func startPhotoCapture() {
        let imagePicker: UIImagePickerController =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .Camera
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        let mediaType:String = String(info["UIImagePickerControllerMediaType"]!)
        if mediaType == "public.image" {
            APIService.Instance.capturePhotoSpot({data, response, error -> Void in
                if(data != nil && data!["spot"] != nil){
                    
                    MeetingService.Instance.hook(data!)
                    
                    let recordingName:String = String(data!["spot"]!["guid"]) + ".jpg"
                    //let pathArray = [self.dirPath, recordingName]
                    //let filePath:NSURL = NSURL.fileURLWithPathComponents(pathArray)!
                    let image = info[UIImagePickerControllerOriginalImage] as? UIImage
                    
                    //let pngImageData = UIImagePNGRepresentation(image)
                    let jpgImageData:NSData = UIImageJPEGRepresentation(image!, 1.0)!
                    
                    let filename:String = HelperService.Instance.getDocumentsDirectoryPath(recordingName)
                    jpgImageData.writeToFile(filename, atomically: true)
                    
                    
                    S3Service.Instance.upload(filename, suffix: recordingName)
                }
            })
        }
    }
    
    
    private func _backToNoRecording(){
        self.audioButton.hidden = false
        self.activeRecLabel.hidden = false
        self.cancelButton.hidden = true
        self.audioRunningButton.hidden = true
    }
    
    private func _setUIToAudioRecording() {
        self.audioButton.hidden = true
        self.activeRecLabel.hidden = true
        self.cancelButton.hidden = false
        self.audioRunningButton.hidden = false
    }
    
    
    // ---------- Audio Delegates
    
    func recordingStarted() {
    }
    
    func recordingFailToStart() {
        _backToNoRecording()
    }
    
    func recordingStopped() {
        _backToNoRecording()
        InternetService.Instance.updateNetworkStatus()
        counter = 0
    }
    
    func currentUploadsChanged() {
        let uploads:Int = S3Service.Instance.uploadsInProgress
        
        dispatch_async(dispatch_get_main_queue(), {
            self.uploadLabel.text = "\(uploads)"
            self.uploadLabel.hidden = uploads == 0
            self.uploadActivity.hidden = uploads == 0
        })
    }
}

