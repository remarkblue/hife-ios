import AWSCore

class S3Service : NSObject, NSFetchedResultsControllerDelegate{
    
    var uploadsInProgress:Int
    var uploadObjectsInProgress:[String:SpotDataStore]
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var progressBlock: AWSS3TransferUtilityUploadProgressBlock?
    
    var delegate:S3ServiceDelegate!
    
    private override init() {
        uploadObjectsInProgress = [String:SpotDataStore]()
        uploadsInProgress = 0
        super.init()
        
        self.completionHandler = { (task, error) -> Void in
            if self.uploadObjectsInProgress[task.key] == nil {
                print("Failed, in progress element not found!")
            } else {
                let obj:SpotDataStore = self.uploadObjectsInProgress[task.key]!
                
                if (error != nil){
                    print("Failed with error: \(error)")
                    obj.status_id = 0
                    self.appDelegate.saveContext()
                    print("0: AWS error:\(obj.name!)")
                }
                    //                else if(self.progressView.progress != 1.0) {
                    //                    self.statusLabel.text = "Failed"
                    //                    NSLog("Error: Failed - Likely due to invalid region / filename")
                    //                }
                else{
                    
                    if self.uploadsInProgress > 0 {
                        self.uploadsInProgress = self.uploadsInProgress - 1
                        self.delegate?.currentUploadsChanged()
                    }
                    obj.status_id = 2
                    self.appDelegate.saveContext()
                    //print("2: Upload Completed on \(obj.name!)")
                    
                    APIService.Instance.mediaSpotUploaded(obj.name!, completionHandler: {data, response, error -> Void in
                        obj.status_id = 3
                        self.appDelegate.saveContext()
                        //print("3: Uploade Fully processed:\(obj.name!)")
                    })
                    
                    let fileManager = NSFileManager.defaultManager()
                    do {
                        try fileManager.removeItemAtPath(obj.path!)
                        self.uploadObjectsInProgress.removeValueForKey(task.key)
                    }
                    catch let error as NSError {
                        print("Ooops! Something went wrong trying to delete path: \(error)")
                    }
                    
                    //print("Success! Upload Completed")
                }
            }
        }
        
        
        //        self.progressBlock = {(task, progress) in
        //            dispatch_async(dispatch_get_main_queue(), {
        //                self.progressView.progress = Float(progress.fractionCompleted)
        //                self.statusLabel.text = "Uploading..."
        //            })
        //        }
        
        managedObjectContext = appDelegate.managedObjectContext
        //let forcedGuid = "c7f6fa5f-5906-442a-a3e9-20b336c4a27b.wav"
        //self._forceUpload(HelperService.Instance.getDocumentsDirectoryPath(forcedGuid),
        //                  name: forcedGuid)
        
    }
    
    private func _forceUpload(fullPath:String, name:String) {
        let fileContent = NSData(contentsOfFile: fullPath)
        
        if(fileContent == nil) {
            print("File NOT found on path:\(fullPath)")
        } else {
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            let testFileURL1 = NSURL(fileURLWithPath: fullPath)
            let uploadRequest1 : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            fileContent!.writeToURL(testFileURL1, atomically: true)
            uploadRequest1.bucket = "hife"
            uploadRequest1.key =  name
            uploadRequest1.body = testFileURL1
            
            let task = transferManager.upload(uploadRequest1)
            print("XX: AWS force submited:\(name)")
            
            uploadsInProgress = uploadsInProgress + 1
            delegate?.currentUploadsChanged()
            task.continueWithBlock { (task) -> AnyObject! in
                self.uploadsInProgress = self.uploadsInProgress - 1
                self.delegate?.currentUploadsChanged()
                if task.error == nil {
                    print("XXX: Forced Upload Completed")
                    
                    APIService.Instance.mediaSpotUploaded(name, completionHandler: {data, response, error -> Void in
                        print("XXXX: Force Upload Fully processed:\(name)")
                    })
                } else {
                    print("Error: \(task.error)")
                }
                return nil
            }
        }
    }
    
    static let Instance = S3Service()
    
    func clearUploadsInProgress() {
        uploadsInProgress = 0
        delegate?.currentUploadsChanged()
    }
    
    func upload(path:String, suffix:String) {
        _tryToUpload(path, suffix: suffix, storedSpotParam: nil)
    }
    
    private func _tryToUploadFromDataStore(thisSpot:SpotDataStore) -> Bool {
        return _tryToUpload(thisSpot.path!, suffix: thisSpot.name!, storedSpotParam: thisSpot)
    }
    
    private func _tryToUpload(path:String, suffix:String, storedSpotParam:SpotDataStore?) -> Bool {
        var storedSpot:SpotDataStore
        
        if storedSpotParam == nil {
            storedSpot = _postPathOnQueue(path, name: suffix)
        } else {
            storedSpot = storedSpotParam!
        }
        
        
        if (SettingsService.Instance.getWifiSetting() &&
            InternetService.Instance.networkStatus != InternetService.Instance.WIFI) {
            return false
        } else {
            let fileContent = NSData(contentsOfFile: path)
            if(fileContent == nil) {
                print("File NOT found on path:\(path)")
                storedSpot.status_id = 404
                self.appDelegate.saveContext()
            } else {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    let expression = AWSS3TransferUtilityUploadExpression()
                    //expression.progressBlock = progressBlock
                    
                    let transferUtility = AWSS3TransferUtility.defaultS3TransferUtility()
                    var type:String
                    
                    if path.hasSuffix(".wav") {
                        type = "audio/*"
                    } else {
                        type = "image/jpeg"
                    }
                    
                    self.uploadObjectsInProgress[storedSpot.name!] = storedSpot
                    
                    transferUtility.uploadData(
                        fileContent!,
                        bucket: "hife",
                        key: suffix,
                        contentType: type,
                        expression: expression,
                        completionHander: self.completionHandler).continueWithBlock { (task) -> AnyObject! in
                            if let error = task.error {
                                print("Error: \(error.localizedDescription)")
                            }
                            if let exception = task.exception {
                                print("Exception: \(exception.description)")
                            }
                            if let _ = task.result {
                                print("Upload Started!")
                            }
                            return nil;
                    }
                    
                    storedSpot.status_id = 1
                    self.appDelegate.saveContext()
                    self.uploadsInProgress = self.uploadsInProgress + 1
                    self.delegate?.currentUploadsChanged()
                }
            }
            return true
        }
    }
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    private var managedObjectContext: NSManagedObjectContext? = nil
    
    
    
    func retryPendingUploads() {
        let fetchRequest = NSFetchRequest(entityName: "SpotDataStore")
        var spotDataStore: [SpotDataStore] = [SpotDataStore]()
        do {
            spotDataStore = try (managedObjectContext!.executeFetchRequest(fetchRequest) as! [SpotDataStore])
        } catch let error as NSError {
            print("Fetch failed: \(error.localizedDescription)")
        }
        for spot in spotDataStore {
            if spot.status_id == 0 ||
                (spot.status_id == 1 && uploadsInProgress == 0){
                //print("0: Trying:\(spot.name!)")
                self._tryToUploadFromDataStore(spot)
            }
        }
        appDelegate.saveContext()
    }
    
    private func _postPathOnQueue(path:String, name:String) -> SpotDataStore {
        let objectConstructed:SpotDataStore = NSEntityDescription.insertNewObjectForEntityForName(
            "SpotDataStore", inManagedObjectContext: self.managedObjectContext!) as! SpotDataStore
        objectConstructed.path = path
        objectConstructed.name = name
        objectConstructed.status_id = 0
        appDelegate.saveContext()
        return objectConstructed
    }
    
    
    
    
    
    
}
