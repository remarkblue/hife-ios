import MapKit

class LocationService: NSObject, CLLocationManagerDelegate {
    
    private var locationManager:CLLocationManager!
    private var currentLocation:CLLocation!
    
    var state:Int
    var geoLocation:[String:String]!
    private let geocoder:CLGeocoder!
    var delegate:LocationServiceDelegate!
    var current:CLLocation!
    
    override private init() {
        state = 0
        geocoder = CLGeocoder()
        super.init()
        locationManager = CLLocationManager()
        currentLocation = CLLocation()
        //kCLLocationAccuracyNearestTenMeters
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.delegate = self
        
        switch (CLLocationManager.authorizationStatus()) {
        case .AuthorizedWhenInUse, .AuthorizedAlways:
            locationManager.startUpdatingLocation()
        case .Denied:
            print("Denied!!!")
        case .NotDetermined:
            fallthrough
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    static let Instance = LocationService()
    
    func locationManager(manager: CLLocationManager,
                         didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .Restricted:
            NSLog("Denied access: Restricted Access to location")
        case .Denied:
            print("Denied!!!")
        case .NotDetermined:
            fallthrough
        default:
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        current = locations.last
        if state == 0 {
            state=1
           
            
            geocoder.reverseGeocodeLocation(current, completionHandler: { (placemarks, e) -> Void in
                if e == nil {
                    let placemark = placemarks!.last! as CLPlacemark
                    self.geoLocation = [
                        "city":     placemark.locality!,
                        "state":    placemark.administrativeArea!,
                        "country":  placemark.country!
                    ]
                    self.delegate?.locationServiceReady()
                    print("GPS ready")
                } else {
                    print("Error:  \(e!.localizedDescription)")
                    self.state = 0
                }
            })
            
        }
    }
    
    func postMeetingEntrance(){
        state=0
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
    }
    
}