import UIKit

class EventTabViewController: UITabBarController {
    
    
    var bindDevice:UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO: Check binding state to disply bind button or account
        
        bindDevice = UIBarButtonItem(title: "claim device", style: .Plain, target: self, action: #selector(EventTabViewController.claimDevice))
        
        self.navigationItem.rightBarButtonItem = bindDevice
        
        self.navigationController!.navigationBar.hidden = false // for navigation bar hide
        self.navigationItem.hidesBackButton = MeetingService.Instance.current == nil
    }
    
    @IBAction func claimDevice() {
        let alert = UIAlertController(title: "Claim Device", message: "Login on the web hife.io and claim all your devices to associate them with your hife account.  Enter binding token:", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.text = ""
        })
        
        
        alert.addAction(UIAlertAction(title: "Bind", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            
            APIService.Instance.bindDevice(textField.text!,
                completionHandler: {data, response, error -> Void in
                    
                    //TODO: success?
                    
            })
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel) { (_) in })
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
}
