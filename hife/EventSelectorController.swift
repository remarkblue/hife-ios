import UIKit

class EventSelectorController:
    UITableViewController,
LocationServiceDelegate {
    
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    var activityItem = UIBarButtonItem()
    
    private var _meetings:[Meeting]!
    
    private var timer:NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
        activityIndicator.hidden = false
        self.activityItem = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.rightBarButtonItem = activityItem
        self.navigationController!.navigationBar.hidden = false // for navigation bar hide
        self.navigationItem.hidesBackButton = MeetingService.Instance.current == nil
        
        LocationService.Instance.delegate = self
        
        if LocationService.Instance.state == 0 {
            print("Waiting for GPS init...")
        } else {
            locationServiceReady()
        }
        _meetings = []
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationItem.hidesBackButton = MeetingService.Instance.current == nil
        //self.navigationController!.navigationBar.
    }
    
    override func viewDidDisappear(animated: Bool) {
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _meetings.count + 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Creat brand new event now"
        default:
            cell.textLabel?.text = "\(_meetings[indexPath.row-1].neighbors.count) / \(_meetings[indexPath.row-1].name)"
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self._transition()
        if indexPath.row == 0 {
            APIService.Instance.newMeetingSpot({data, response, error -> Void in
                MeetingService.Instance.hook(data!)
            })
        } else {
            APIService.Instance.joinSpot(_meetings[indexPath.row-1].guid,
                                         completionHandler: {data, response, error -> Void in
                                            MeetingService.Instance.hook(data!)
            })
        }
    }
    
    private func _transition() {
        performSegueWithIdentifier("goToEvent", sender: nil)
    }
    
    func locationServiceReady(){
        getJoinable()
        if self.timer != nil {
            self.timer.invalidate()
        }
        self.timer = NSTimer.scheduledTimerWithTimeInterval(6.0,
                                                            target: self,
                                                            selector: "getJoinable",
                                                            userInfo: nil, repeats: true)
    }
    
    func getJoinable(){
        APIService.Instance.getJoinableMeetings({data, response, error -> Void in
            self._meetings = []
            if(data!["object"] != nil && data!["object"]?.count > 0) {
                let meetings = data!["object"] as! [Dictionary<String, AnyObject>]
                for item in meetings {
                    let meeting = Meeting(meetingData: item)
                    self._meetings.append(meeting)
                }
            }
            self.tableView.reloadData()
        })
    }
    
    
}
