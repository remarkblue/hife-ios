import Locksmith
import Foundation

class SettingsService: NSObject,NSFetchedResultsControllerDelegate {
    
    var deviceUUID: String!
    var deviceName: String!
    var deviceSystemName: String!
    var deviceSystemVersion: String!
    
    
    var maxMinutes = 120
    private let _defaultTimeOutAudioMinutes = 30
    
    private let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    private var managedObjectContext: NSManagedObjectContext? = nil
    
    static let Instance = SettingsService()
    
    override init() {
        let dictionary = Locksmith.loadDataForUserAccount("hifeAccount")
        let device = UIDevice.currentDevice()
        
        super.init()
        
        managedObjectContext = appDelegate.managedObjectContext
        let settings:[SettingDataStore] = _getFetchedData()
        
        if dictionary != nil && dictionary!["uuid"] != nil &&
            (String(dictionary!["uuid"])).characters.count > 0 {
            deviceUUID = String(dictionary!["uuid"]!)
        } else {
            var uuid:String = (device.identifierForVendor?.UUIDString)!
            if(settings.count > 0 && settings[0].device_uuid != nil &&
                settings[0].device_uuid?.characters.count > 1) { //check if DB available?
                uuid = settings[0].device_uuid!
            }
            
            do {
                try Locksmith.saveData([
                    "uuid": uuid],
                                       forUserAccount: "hifeAccount")
            } catch {
                print("Error while saving on keychain")
            }
            deviceUUID = (device.identifierForVendor?.UUIDString)!
        }
        
        deviceName = device.name
        deviceSystemName = device.systemName
        deviceSystemVersion = device.systemVersion
        
        if(settings.count < 1) {
            setWifiSetting(true)
        }
    }
    
    private func _getFetchedData() -> [SettingDataStore] {
        let fetchRequest = NSFetchRequest(entityName: "SettingDataStore")
        var settingDataStore: [SettingDataStore] = [SettingDataStore]()
        
        do {
            settingDataStore = try (managedObjectContext!.executeFetchRequest(fetchRequest) as! [SettingDataStore])
        } catch let error as NSError {
            print("Fetch failed: \(error.localizedDescription)")
        }
        return settingDataStore
    }
    
    func setWifiSetting(only_wifi:Bool) {
        let setting:[SettingDataStore] = _getFetchedData()
        
        if (setting.count > 0) { //Update
            setting[0].upload_only_wifi = only_wifi ? 1 : 0
        } else { // First time setting value
            let objectConstructed:SettingDataStore = NSEntityDescription.insertNewObjectForEntityForName(
                "SettingDataStore", inManagedObjectContext: self.managedObjectContext!) as! SettingDataStore
            objectConstructed.upload_only_wifi = only_wifi ? 1 : 0
            objectConstructed.audio_time_out = _defaultTimeOutAudioMinutes;
        }
        
        appDelegate.saveContext()
    }
    
    func getWifiSetting() -> Bool {
        let setting:[SettingDataStore] = _getFetchedData()
        
        if (setting.count > 0) {
            return setting[0].upload_only_wifi == 1
        } else {
            return true
        }
    }
    
    
    
    
    func setAudioTimeoutSetting(minutes:NSNumber) {
        let intminutes = Int(minutes)
        
        if(intminutes >= 0 || intminutes <= maxMinutes) {
            let setting:[SettingDataStore] = _getFetchedData()
            
            if (setting.count > 0) { //Update
                setting[0].audio_time_out = minutes
            } else { // First time setting value
                let objectConstructed:SettingDataStore = NSEntityDescription.insertNewObjectForEntityForName(
                    "SettingDataStore", inManagedObjectContext: self.managedObjectContext!) as! SettingDataStore
                objectConstructed.audio_time_out = minutes
            }
            appDelegate.saveContext()
        } else {
            print("invalid value")
        }
    }
    
    func getAudioTimeoutSetting() -> Double {
        let setting:[SettingDataStore] = _getFetchedData()
        
        if (setting.count > 0) {
            return Double(setting[0].audio_time_out!)
        } else {
            return Double(_defaultTimeOutAudioMinutes)
        }
    }
    
    
}
