//
//  SpotDataStore+CoreDataProperties.swift
//  hife
//
//  Created by MAURICIO MARTINEZ INGUANZO on 4/10/16.
//  Copyright © 2016 inguansoft. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SpotDataStore {

    @NSManaged var path: String?
    @NSManaged var name: String?
    @NSManaged var status_id: NSNumber?

}
