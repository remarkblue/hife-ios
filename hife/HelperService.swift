class HelperService {
    
    private init() {
        
    }
    
    static let Instance = HelperService()
    
    func getDocumentsDirectoryPath(fileName:String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return NSString(string: documentsDirectory).stringByAppendingPathComponent(fileName)
    }
    
    
}