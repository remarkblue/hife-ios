import MapKit
import Alamofire
import SystemConfiguration

class InternetService {
    private let API_SERVER_DOMAIN:String = "https://api.hife.io/"
    //private let API_SERVER_DOMAIN:String = "http://192.168.0.133:3000/"
    //private let API_SERVER_DOMAIN:String = "http://192.168.2.28:3000/"

    private init() {
    }
    
    static let Instance = InternetService()
    
    var networkStatus:Int!
    
    let OFFLINE:Int = 0,
    WIFI:Int = 1,
    CELL:Int = 2
    
    func updateNetworkStatus() {
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            //print("Unable to create Reachability")
            self.networkStatus = self.OFFLINE
            return
        }
        
        reachability.whenReachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            dispatch_async(dispatch_get_main_queue()) {
                if reachability.isReachableViaWiFi() {
                    self.networkStatus = self.WIFI
                    //print("Reachable via WiFi")
                } else {
                    self.networkStatus = self.CELL
                    //print("Reachable via Cellular")
                }
            }
        }
        reachability.whenUnreachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            dispatch_async(dispatch_get_main_queue()) {
                self.networkStatus = self.OFFLINE
                //print("Not reachable")
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            self.networkStatus = self.OFFLINE
            print("Unable to start notifier")
        }
    }
    
    func isHot() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func endPointURL(method:String) -> String {
        return API_SERVER_DOMAIN + method
    }

    func delete(params : Dictionary<String, Dictionary<String, String>>, endPoint: String,
        completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        if(isHot()) {
            _request("DELETE", params: params, endPoint: endPoint, completionHandler: completionHandler)
        } else {
        //TODO: cache it to send later
        }
    }
    
    func post(params : Dictionary<String, Dictionary<String, String>>, endPoint: String,
        completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        if(isHot()) {
            _request("POST", params: params, endPoint: endPoint, completionHandler: completionHandler)
        } else {
            //TODO: cache it to send later
        }
    }

    func put(params : Dictionary<String, Dictionary<String, String>>, endPoint: String,
        completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
        if(isHot()) {
            _request("PUT", params: params, endPoint: endPoint, completionHandler: completionHandler)
        } else {
            //TODO: cache it to send later
        }
    }

    
    
    
    private func _request(httpMETHOD:String,
                          params : Dictionary<String, Dictionary<String, String>>,
                          endPoint: String,
                          completionHandler: (NSDictionary?, NSURLResponse?, NSError?) -> Void) {
            let url:String = endPointURL(endPoint)
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            let session = NSURLSession.sharedSession()

            request.HTTPMethod = httpMETHOD
            do {
                try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
            } catch {
                print("Error on dataWithJSONObject")
            }
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if(response != nil && data != nil) {
                //print("Response: \(response)")
                //var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                var json:NSDictionary = NSDictionary()
                //print("Body: \(strData)")
                
                do {
                    try json = NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSDictionary
                    //try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves)
                    completionHandler(json, response, error)
                } catch {
                    print("Try JSON failed")
                }
            } else {
                print("Missing response")
            }
            
        })
        
        task.resume()
    }



}